# funding-pipeline

The goal of this project is to integrate analytical insights and various design ideas to develop a strategy (and hopefully an implementation) for an effective and efficient funding pipeline for the EA movement. At the moment please refer to the [wiki](https://gitlab.com/effective-altruism/funding-pipeline/wikis/home) for more information.